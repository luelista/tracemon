var app = module.exports = require('appjs');

app.serveFilesFrom(__dirname + '/content');

console.log("111");

var traceProtocol = require('./traceProtocol');

console.log("222");

var menubar = createMenu();

menubar.on('select',function(item){
  console.log("menu item "+item.label+" clicked");
});
/*
var trayMenu = app.createMenu([{
  label:'Show',
  action:function(){
    window.frame.show();
  },
},{
  label:'Minimize',
  action:function(){
    window.frame.hide();
  }
},{
  label:'Exit',
  action:function(){
    window.close();
  }
}]);

var statusIcon = app.createStatusIcon({
  icon:'./data/content/icons/32.png',
  tooltip:'AppJS Hello World',
  menu:trayMenu
});*/

var splash = splashScreen(runServer);

function runServer() {
  console.log("starting trace server....");

  traceProtocol.on('error', function(err) {
    console.log("Error starting server: ", err);
    var alertBox1 = alertBox(err);
    if(splash) alertBox1.on('ready', function(){ splash.close(); splash=null; });
  });
  traceProtocol.on('listening', function() {
    openMainWindow();
    if(splash) window.on('ready', function() { splash.close(); splash=null; });
  })

  traceProtocol.initServer();
  console.log("done!");
}

function alertBox(text) {
  var win2 = app.createWindow({
    width  : 400,
    height : 250,
    icons  : __dirname + '/content/icons',
    url    : 'http://appjs/test.html'
  });

  win2.on('create', function(){
    console.log("win2 Created");
    win2.frame.show();
    win2.frame.center();
  });
  win2.on('ready', function(){
    win2.document.getElementsByTagName("h2")[0].innerHTML = "..."+text;
  });
  return win2;
}

var window;
function openMainWindow() {
  window = app.createWindow({
    width  : 640,
    height : 460,
    icons  : __dirname + '/content/icons'
  });

  window.on('create', function(){
    console.log("Window Created");
    window.frame.show();
    window.frame.center();
    window.frame.setMenuBar(menubar);
  });

  window.on('ready', function(){
    console.log("Window Ready");

    window.process = process;
    window.module = module;
    window.traceProtocol = traceProtocol;

    function F12(e){ return e.keyIdentifier === 'F12' }
    function Command_Option_J(e){ return e.keyCode === 74 && e.metaKey && e.altKey }

    window.addEventListener('keydown', function(e){
      if (F12(e) || Command_Option_J(e)) {
        window.frame.openDevTools();
      }
      console.log(e.keyCode);
      if (e.keyIdentifier === 'F1') openTestWindow();
      if (e.keyCode === 81) window.close();
      if (e.keyIdentifier === 'F2') window.frame.openDialog( { type : 'open', title : 'Open...', multiSelect : false, acceptTypes : { 'Images' : [ '*.jpg' ] }, dirSelect : false, initialValue : '/' });
    });

    //window.frame.openDevTools();
    console.log(window.ontraceready);
    window.ontraceready();
  });

  window.on('close', function(){
    console.log("Window Closed");
  });
}

//-->
//--> M E N U   B A R


function createMenu() {
  return app.createMenu([{
    label:'&File',
    submenu:[
      {
        label:'Test Menu Item',
        action: function(){
          window.close();
        }
      },
      {
        label:'E&xit',
        action: function(){
          window.close();
        }
      }
    ]
  },{
    label:'&Window',
    submenu:[
      {
        label:'Fullscreen',
        action:function(item) {
          window.frame.fullscreen();
          console.log(item.label+" called.");
        }
      },
      {
        label:'Minimize',
        action:function(){
          window.frame.minimize();
        }
      },
      {
        label:'Maximize',
        action:function(){
          window.frame.maximize();
        }
      },{
        label:''//separator
      },{
        label:'Restore',
        action:function(){
          window.frame.restore();
        }
      }
    ]
  }]);
}

function splashScreen(callback) {
  var win2 = app.createWindow({
    width  : 450,
    height : 300,
    icons  : __dirname + '/content/icons',
    url    : 'http://appjs/splash.html'
  });

  win2.on('create', function(){
    console.log("splash screen Created");
    win2.frame.show();
    win2.frame.center();
  });
  win2.on('ready', function(){
    setTimeout(callback, 1000);
  });
  return win2;
}
