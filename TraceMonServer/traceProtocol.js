

var net = require('net');

function subscribeTo(server) {
  net.connect(server, socketHandler);
}
function registerTo(server) {
  net.connect(server, function(socket) {
    socketHandler(socket);
    subscribers.push(socket);
    socket.write("Register: trace.js broadcaster\r\n");
  });
}

function emitLines (stream, lineFeedChar) {
  var backlog = ''
  stream.on('data', function (data) {
    //console.log("data in "+data.length+" bytes");
    backlog += data
    var n = backlog.indexOf(lineFeedChar)
    // got a \n? emit one or more 'line' events
    while (~n) {
      stream.emit('line', backlog.substring(0, n))
      backlog = backlog.substring(n + lineFeedChar.length)
      n = backlog.indexOf(lineFeedChar)
    }
  })
  stream.on('end', function () {
    if (backlog) {
      stream.emit('line', backlog)
    }
  })
}


var subscribers = [];

function transmit(line) {
  for(var i in subscribers) {
    subscribers[i].write(line+"\r\n");
  }
}


function socketHandler(socket) {
  var regId = false, connId = (+new Date())+'-'+socket.remotePort;
  socket.setEncoding('ascii');
  emitLines(socket, "\r\n");

  socket.on('line', function (line) {
    console.log(line);
    var idx = line.indexOf(":");
    if (idx == -1) { socket.write("Error: invalid syntax\r\n"); return; }
    var cmd = line.substring(0,idx);
    var cnt = line.substring(idx+1);
    switch(cmd) {
      case "Register":
        if(!regId) regId = "["+socket.remoteAddress+"]"+cnt;
        listener('newTarget', null, regId);
        break;
      case "Trace":
        if (!regId) return;
        var val=cnt.split(/\|/);
        listener('tl', 't:'+regId, {t:regId,c:connId,p:val});
        transmit(line);
        break;
      case "Subscribe":
        var i = subscribers.indexOf(socket);
        if (i==-1)    {    subscribers.push(socket); socket.write("OK: subscribed\r\n"); } else socket.write("Error: Already subscribed :-)\r\n");
        break;
      default:
        socket.write("Error: not implemented\r\n");
        break;
    }
    
  });

  socket.on('error', function (err) {
    console.log("socketError: " + err);
    var i = subscribers.indexOf(socket);
    if (i>-1) delete subscribers[i];
  });
  socket.resume();
}

var listener = function() {};
function listen(new_listener) {
  listener = new_listener;
}

function initServer(port) {
  if(!port) port = 10777;
  var server = net.createServer(function (socket) {
    socketHandler(socket);
  });
  server.listen(port);
  console.log("Listening on port:"+port+" ...");
  return server;
}

module.exports = { initServer: initServer, transmit: transmit, listen: listen };
