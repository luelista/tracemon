
var traceProtocol = require('../TraceMonitor.app/Contents/data/traceProtocol');

var crypto = require ('crypto');
var fs = require ('fs');

var privateKey = fs.readFileSync(__dirname + '/server.key').toString();
var certificate = fs.readFileSync(__dirname + '/server.crt').toString();

var URL_ROOT = "https://teamwiki.de:8443/";

var express = require('express')
var app = express()
  , http = require('http')
  , https = require('https')
  , server = https.createServer({ key: privateKey, cert: certificate }, app);


var io = require('socket.io').listen(server);

var runtimeId = Math.floor(Math.random()*100000)+1;

var lastUpload = "";

io.set('log level', 2); 
server.listen(10443);

app.use(express.bodyParser());

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});
app.get('/client.js', function (req, res) {
  res.sendfile('TraceMonitor.app/Contents/data/content/client.js', { root: __dirname + '/../' });
});
app.get(/\/trace_icons\/([a-z0-9]+)/, function (req, res) {
  //console.log(req)
  res.sendfile(__dirname + '/trace_icons/' + req.params[0] + '.ico');
});


io.sockets.on('connection', function (socket) {
  socket.on('listenOn', function(target) {
    socket.join('t:'+target);
  });
  socket.on('subscribeTo', function(server) {
    subscribeTo(server);
  });
  socket.on('registerTo', function(server) {
    registerTo(server);
  });
});

function subscribeTo(server) {
  net.connect(server, socketHandler);
}
function registerTo(server) {
  net.connect(server, function(socket) {
    socketHandler(socket);
    subscribers.push(socket);
    socket.write("Register: trace.js broadcaster\r\n");
  });
}

function emitLines (stream, lineFeedChar) {
  var backlog = ''
  stream.on('data', function (data) {
    //console.log("data in "+data.length+" bytes");
    backlog += data
    var n = backlog.indexOf(lineFeedChar)
    // got a \n? emit one or more 'line' events
    while (~n) {
      stream.emit('line', backlog.substring(0, n))
      backlog = backlog.substring(n + lineFeedChar.length)
      n = backlog.indexOf(lineFeedChar)
    }
  })
  stream.on('end', function () {
    if (backlog) {
      stream.emit('line', backlog)
    }
  })
}

traceProtocol.initServer();
traceProtocol.listen(function(message, channel, data) {
  if(channel) io.sockets.to(channel).emit(message, data);
  else io.sockets.emit(message, data);
})



